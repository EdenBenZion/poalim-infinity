package com.poalim.assignment.fibonacci;

public interface IFibonacciCalculator {

	public long calculateByIndex(int index);
}
