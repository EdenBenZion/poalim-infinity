package com.poalim.assignment.fibonacci.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.poalim.assignment.fibonacci.IFibonacciController;
import com.poalim.assignment.fibonacci.dto.FibonacciResponse;

@RestController
public class FibonacciController implements IFibonacciController {

	@Autowired
	private FibonacciRepository fibonacciRepository; 

	@Autowired
	private FibonacciCalculator fibonacciCalculator; 
	
	@GetMapping("/{index}")
	public ResponseEntity<FibonacciResponse> fibonacciNumberByIndex(@PathVariable int index) {
		// TODO use the fibonacciRepository and fibonacciCalculator instances to return FibonacciResponse to the client 
		return null;
	}

}
